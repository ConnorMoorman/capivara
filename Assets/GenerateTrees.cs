﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class GenerateTrees : MonoBehaviour {

    GameObject treeTemplate;
    GameObject Trees;

    int treeCount = 0;
    float topTreeZ = 180f;
    float treeSpawnX = 250f;
    float awaitTime;
    float lastCall;

	// Use this for initialization
	void Start () {
        treeTemplate = GameObject.Find("Trees/TreeTemplate");
        Trees = GameObject.Find("Trees");

        treeTemplate.SetActive(false);

        lastCall = 0f;
        awaitTime = Random.Range(1f, 2f);
        treeCount = 0;
    }
	
	// Update is called once per frame
	void Update () {

        lastCall += Time.deltaTime;

		if(lastCall > awaitTime)
        {
            createTree();
            awaitTime = Mathf.Max(1, (2.25f - (.015f * treeCount)));
            //Debug.Log("The next spawn time is: " + awaitTime);
            lastCall = 0;
        }
	}

    void createTree()
    {
        if (treeTemplate != null && !Advertisement.isShowing)
        {
            // Create our new trees
            GameObject newTree1 = Instantiate(treeTemplate);
            GameObject newTree2 = Instantiate(treeTemplate);
            newTree1.tag = "Tree";
            newTree1.name = "TopTree";
            newTree2.tag = "Tree";
            newTree2.name = "BottomTree";

            // Add the trees to the Trees hierarchy.
            newTree1.transform.SetParent(Trees.transform);
            newTree2.transform.SetParent(Trees.transform);

            float topTreeY = 0;
            float bottomTreeY = 0;

            // -- Need to decide where to place this Tree.
            topTreeY = Random.Range(50f, 100f);
            bottomTreeY = topTreeY + Random.Range(-175f, -135f);

            // -- Set the trees position.
            newTree1.transform.position = new Vector3(treeSpawnX, topTreeY, 0f);
            newTree2.transform.position = new Vector3(treeSpawnX, bottomTreeY, 0f);

            newTree1.transform.localPosition = new Vector3(newTree1.transform.localPosition.x,
                newTree1.transform.localPosition.y, 0f);

            newTree1.transform.Rotate(new Vector3(0f, 0f, topTreeZ));

            // -- Allow our trees to move.
            newTree1.GetComponent<Tree>().enabled = true;
            newTree2.GetComponent<Tree>().enabled = true;

            // -- Set the tree to be Active.
            newTree1.SetActive(true);
            newTree2.SetActive(true);

            // -- Increment our tree count by 1.
            treeCount++;
        }
    }

    void initializeTrees()
    {
        if (treeTemplate != null)
        {
            // -- Create our new trees
            GameObject newTree1 = Instantiate(treeTemplate);
            GameObject newTree2 = Instantiate(treeTemplate);
            newTree1.tag = "Tree";
            newTree1.name = "TopTree";
            newTree2.tag = "Tree";
            newTree2.name = "BottomTree";

            // Add the trees to the Trees hierarchy.
            newTree1.transform.parent = Trees.transform;
            newTree2.transform.parent = Trees.transform;

            float topTreeY = 0;
            float bottomTreeY = 0;

            // -- Need to decide where to place this Tree.
            topTreeY = Random.Range(50f, 100f);
            bottomTreeY = topTreeY + Random.Range(-200f, -165f);

            float offset = 0f;

            // -- Set the trees position.
            newTree1.transform.position = new Vector3(1f + offset, topTreeY, 0f);
            newTree2.transform.position = new Vector3(1f + offset, bottomTreeY, 0f);

            newTree1.transform.Rotate(new Vector3(0f, 0f, topTreeZ));

            // Set the tree to be Active.
            newTree1.SetActive(true);
            newTree2.SetActive(true);

            treeCount++;
        }
    }

    public void resetTreeCount()
    {
        treeCount = 0;
    }
}
