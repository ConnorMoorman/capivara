﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour {

	// Use this for initialization
	void Begin() {
        GameObject startMenu = GameObject.Find("StartMenuCanvas");
        GameObject capivara = GameObject.Find("GameCanvas/Capybara");
        GameObject trees = GameObject.Find("GameCanvas/Trees");

        capivara.GetComponent<Falling>().enabled = true;

        trees.GetComponent<GenerateTrees>().enabled = true;

        startMenu.SetActive(false);
	}
}
