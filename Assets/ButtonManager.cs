﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour {

    public GameObject gameManager;
    
    GameObject trees;

    // Use this for initialization
    void Start () {
        trees = GameObject.Find("GameCanvas/Trees");
    }

    public void startPressed()
    {
        gameManager.GetComponent<PlayerManager>().startPlayerMovement();

        trees.GetComponent<GenerateTrees>().enabled = true;

        gameManager.GetComponent<UIManager>().hideStartMenuParent();
    }

    public void resetPressed()
    {
        // -- Grab all of the active trees.
        GameObject[] treeObjects = GameObject.FindGameObjectsWithTag("Tree");

        // -- Destroy each of the active trees.
        foreach(GameObject tree in treeObjects)
        {
            Destroy(tree);
        }

        GameObject.Find("GameCanvas/Trees").GetComponent<GenerateTrees>().resetTreeCount();

        // -- Need to reset the Capivara's position.
        gameManager.GetComponent<PlayerManager>().resetPlayerPosition();

        // -- Need to start the Capivara's functions again.
        gameManager.GetComponent<PlayerManager>().startPlayerMovement();

        // -- Reset the player's score.
        gameManager.GetComponent<ScoreManager>().ResetScore();

        // -- Reset the game to not be over.
        gameManager.GetComponent<GameOverManager>().setGameNotOver();

        // -- Hide the end game parents' children.
        gameManager.GetComponent<UIManager>().hideEndGameParent();

        // -- Start the trees.
        trees.GetComponent<GenerateTrees>().enabled = true;
    }
}
