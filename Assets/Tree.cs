﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class Tree : MonoBehaviour {

    public GameObject gameManager;

    float movingSpeed = 1.25f;
    private bool scored;

    private void Start()
    {
        scored = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!Advertisement.isShowing)
        {
            if (!scored && gameObject.transform.localPosition.x < -275f)
            {
                // -- To prevent double points, only score the top trees.
                // TODO: Is there a better way to handle this?
                if (gameObject.transform.position.y > 0)
                {
                    gameManager.GetComponent<ScoreManager>().UpdateScore();

                    if(!gameManager.GetComponent<SoundManager>().isSoundMuted())
                        gameObject.GetComponent<AudioSource>().Play();
                }

                scored = true;
            }

            if (gameObject.transform.localPosition.x < -500f)
            {
                Destroy(gameObject);
            }

            gameObject.transform.position = new Vector3(gameObject.transform.position.x - movingSpeed,
                gameObject.transform.position.y);
        }
	}
}
