﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckColliders : MonoBehaviour {

    public GameObject gameManager;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ( !gameManager.GetComponent<GameOverManager>().isGameOver()
            && (collision.collider.tag.CompareTo("Tree") == 0 ) )
        {
            // -- Disable the movement of the capivara
            gameManager.GetComponent<PlayerManager>().stopPlayerBouncing();

            // -- Show End Game controls.
            gameManager.GetComponent<UIManager>().showEndGameParent();

            // -- Stop the spawn of trees.
            GameObject.Find("GameCanvas/Trees").GetComponent<GenerateTrees>().enabled = false;

            gameManager.GetComponent<GameOverManager>().setGameOver();
        }
    }
}
