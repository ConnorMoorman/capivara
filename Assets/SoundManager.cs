﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

    public AudioSource audioSource;
    public bool soundMuted;

    private GameObject muteSpeaker;

    private void Start()
    {
        soundMuted = false;
        muteSpeaker = GameObject.Find("GameCanvas/MuteButton");
    }

    public void muteSound()
    {
        soundMuted = !soundMuted;
    }

    /// <summary>
    /// Handles the muting of the sound and changes the 
    /// Speaker icon accordingly.
    /// </summary>
    /// <returns></returns>
    
    public void handleMute()
    {
        audioSource.Stop();

        muteSound();

        if(isSoundMuted())
        {
            muteSpeaker.GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/Sound/sound_muted");
        }
        else
        {
            muteSpeaker.GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/Sound/sound_not_muted");
        }
    } 


    public bool isSoundMuted()
    {
        return soundMuted;
    }

    public void playJumpSound()
    {
        playSound("Sounds/CasualGameSounds/DM-CGS-32");
    }

    public void playLasqueiraSound()
    {
        playSound("Sounds/Lasqueira/laqueira_fade");
    }

    public void playPointSound()
    {
        playSound("Sounds/CasualGameSounds/DM-CGS-45");
    }

    private void playSound(string path)
    {
        if(!Advertisement.isShowing && !isSoundMuted())
        {
            audioSource.clip = Resources.Load(path) as AudioClip;
            audioSource.Play();
        }
        else
        {
            if (audioSource.isPlaying)
                audioSource.Stop();
        }
    }
}
