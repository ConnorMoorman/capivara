﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles the displayment of the start menu and the end game menu.
/// </summary>
public class UIManager : MonoBehaviour {

    public GameObject startMenuParent;
    public GameObject endGameParent;
    public Text scoreText;

    private void Start()
    {
        updateVersion();
    }

    private void updateVersion()
    {
        GameObject.Find("GameCanvas/Version").GetComponent<Text>().text = "version: " + Application.version;
    }

    // -- Menu management
    public void showStartMenuParent()
    {
        startMenuParent.SetActive(true);
    }

    public void hideStartMenuParent()
    {
        startMenuParent.SetActive(false);
    }

    public void showEndGameParent()
    {
        endGameParent.SetActive(true);
    }

    public void hideEndGameParent()
    {
        endGameParent.SetActive(false);
    }

    // -- UI Score management.
    public void updateScoreText(int score)
    {
        scoreText.text = "Score: " + score.ToString();
    }
}
