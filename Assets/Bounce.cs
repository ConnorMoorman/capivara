﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class Bounce : MonoBehaviour {

    public float bounceSpeed = 25f * 4;

    public GameObject GameManager;

    // Update is called once per frame
    void Update () {

        if( !Advertisement.isShowing && Input.GetKeyDown(KeyCode.Space) || 
            (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) )
        {
            gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x,
                gameObject.transform.localPosition.y + bounceSpeed);

            GameManager.GetComponent<SoundManager>().playJumpSound();
        }
    }
}
