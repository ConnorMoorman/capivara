﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallEndgameSound : MonoBehaviour {

    public GameObject GameManager;

    public void OnEnable()
    {
        GameManager.GetComponent<SoundManager>().playLasqueiraSound();
    }
}
