﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class Falling : MonoBehaviour {

    public GameObject gameManager;

    float fallingSpeed = 1f;
    float rotationSpeed = 15f;

    // Update is called once per frame
    void Update() {

        if(!Advertisement.isShowing)
        {
            if (gameObject.transform.localPosition.y > -800)
            {
                gameObject.transform.position = new Vector3(gameObject.transform.position.x,
                            gameObject.transform.position.y - fallingSpeed, 0f);

                if (gameManager.GetComponent<GameOverManager>().isGameOver())
                {
                    fallingSpeed = 2f;

                    gameObject.transform.Rotate(
                        new Vector3(0f, 0f, gameObject.transform.position.z + rotationSpeed)
                        );
                }
            }
        }
    }

    public void resetFallingSpeed()
    {
        fallingSpeed = 1f;
    }
}
