﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class PlayerManager : MonoBehaviour {

    public GameObject player;
    public float fallingSpeed;

    Vector3 capivaraStartPos;
    Vector3 capivaraStartLocalPos;

    private int numTimesReset;
    private float timePlayed;
    private bool applicationPaused;

    private void Start()
    {
        capivaraStartPos = player.transform.position;
        capivaraStartLocalPos = player.transform.localPosition;

        fallingSpeed = 1f;
        numTimesReset = 0;
        timePlayed = 0;
        applicationPaused = false;
    }

    private void Update()
    {
        // -- Do NOT run the timer while the app is paused or an ad is showing.
        if(!applicationPaused && !Advertisement.isShowing)
        {
            timePlayed += Time.deltaTime;
        }
    }

    private void OnApplicationPause(bool pause)
    {
        applicationPaused = pause;
    }

    public void startPlayerMovement()
    {
        player.GetComponent<Falling>().enabled = true;
        player.GetComponent<Bounce>().enabled = true;
    } 
    public void stopPlayerMovement()
    {
        player.GetComponent<Falling>().enabled = false;
        player.GetComponent<Bounce>().enabled = false;
    }

    public void stopPlayerBouncing()
    {
        player.GetComponent<Bounce>().enabled = false;
    }

    public Vector3 getCurrentPostion()
    {
        return player.transform.position;
    }

    public Vector3 getCurrentLocalPosition()
    {
        return player.transform.localPosition;
    }

    public void resetPlayerPosition()
    {
        // TODO: Oddly, to reset the player, these coodinates need to be used.
        player.transform.localPosition = capivaraStartLocalPos;
        player.transform.position = capivaraStartPos;

        // -- Need to reset the Capivara's rotation.
        player.transform.rotation = Quaternion.identity;

        resetFallingSpeed();

        numTimesReset++;

        if(numTimesReset > 6 || timePlayed > 210)
        {
            if(Advertisement.IsReady() && Advertisement.isSupported)
            {
                Advertisement.Show();
            }

            numTimesReset = 0;
            timePlayed = 0f;
        }
    }

    // -- Speed management.
    public void setFallingSpeed(float speed)
    {
        fallingSpeed = speed;
    }

    public void resetFallingSpeed()
    {
        fallingSpeed = 1f;
    }
}
