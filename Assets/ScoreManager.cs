﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
    
    private bool scoreUpdated;
    private int score;

    public GameObject gameManager;

    // Use this for initialization
    void Start()
    {
        // -- Setup some data.
        scoreUpdated = false;
        score = 0;
    }

    // Update is called once per frame
    void Update () {

        // -- Check to see if the score was updated and if the game is still going.
        if (scoreUpdated && !gameManager.GetComponent<GameOverManager>().isGameOver())
        {
            gameManager.GetComponent<UIManager>().updateScoreText(score);
            scoreUpdated = false;
        }
    }

    /// <summary>
    /// Increments the score of the game by 1.
    /// </summary>
    public void UpdateScore()
    {
        score++;
        scoreUpdated = true;
    }

    /// <summary>
    /// Resets the score of the game.
    /// </summary>
    public void ResetScore()
    {
        score = 0;
        scoreUpdated = true;
    }
}
