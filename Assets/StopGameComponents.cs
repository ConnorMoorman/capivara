﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopGameComponents : MonoBehaviour {

    public GameObject gameManager;

    GameObject[] trees;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // -- Stop the spawning of trees.
        GameObject.Find("Trees").GetComponent<GenerateTrees>().enabled = false;

        trees = GameObject.FindGameObjectsWithTag("Tree");

        // -- Stop the trees.
        foreach (var tree in trees)
        {
            tree.GetComponent<Tree>().enabled = false;
        }

        // -- Game over.
        gameManager.GetComponent<GameOverManager>().setGameOver();
        
        // -- Stop the bouncing.
        gameManager.GetComponent<PlayerManager>().stopPlayerBouncing();

        // -- Show End Game controls.
        gameManager.GetComponent<UIManager>().showEndGameParent();
    }
}
