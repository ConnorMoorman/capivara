﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour {

    private bool gameOver;

	// Use this for initialization
	void Start () {
        gameOver = false;
	}

    public void setGameOver()
    {
        gameOver = true;
    }

    public void setGameNotOver()
    {
        gameOver = false;
    }

    public bool isGameOver()
    {
        return gameOver;
    }
}
